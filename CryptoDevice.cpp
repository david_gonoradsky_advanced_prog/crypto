#include "CryptoDevice.h"
#include <md5.h>
#include "hex.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];


CryptoDevice::CryptoDevice(){}
CryptoDevice::~CryptoDevice(){}


string CryptoDevice::encryptAES(string plainText)
{
	string cipherText;
	CryptoPP::AES::Encryption AESEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_CTS_Mode_ExternalCipher::Encryption CBSEncryption(AESEncryption, iv);
	CryptoPP::StreamTransformationFilter STFEncryption(CBSEncryption, new CryptoPP::StringSink(cipherText));
	STFEncryption.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	STFEncryption.MessageEnd();
	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{
	string decryptedText;
	CryptoPP::AES::Decryption AESDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_CTS_Mode_ExternalCipher::Decryption CBSDecryption(AESDecryption, iv);
	CryptoPP::StreamTransformationFilter STFDecryption(CBSDecryption, new CryptoPP::StringSink(decryptedText));
	STFDecryption.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.length() + 1);
	STFDecryption.MessageEnd();
	return decryptedText;
}

string encryptMD5(string plaintext)
{
	string encrypted;
	CryptoPP::SHA256 hash;
	CryptoPP::StringSource s(plaintext, true, new CryptoPP::HashFilter(hash, new CryptoPP::HexEncoder(new CryptoPP::StringSink(encrypted))));
	return encrypted;
}